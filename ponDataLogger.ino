// _________________________________________________________________________________________ //
//|                                                                                         |//
//|                                       Data logger                                       |//
//|                                       version 0.1                                       |//
//|                                                                                         |//
//|                                 (C) 2020 Michele Pirola                                 |//
//|                                                                                         |//
//|   -----------------------------------------------------------------------------------   |//
//|                                                                                         |//
//|          This program is free software; you can redistribute it and/or modify           |//
//|          it under the terms of the GNU General Public License as published by           |//
//|           the Free Software Foundation; either version 3 of the License, or             |//
//|                          (at your option) any later version.                            |//
//|                                                                                         |//
//|             This program is distributed in the hope that it will be useful,             |//
//|             but WITHOUT ANY WARRANTY; without even the implied warranty of              |//
//|              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               |//
//|                      GNU General Public License for more details.                       |//
//|                                                                                         |//
//|            You should have received a copy of the GNU General Public License            |//
//|         along with this program; if not, write to the Free Software Foundation,         |//
//|            Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA            |//
//|_________________________________________________________________________________________|//
//                                                                                           //

//version ______________________________________________________________________________________________________________________/
#define VERSION "0.1"               //version number

//libraries ____________________________________________________________________________________________________________________/
#include <Wire.h>                   //i2c library (for RTC communication)
#include <OneWire.h>                //one-wire library (for DS18B20 communication)
#include <RTClib.h>                 //RTC library
#include <LiquidCrystal.h>          //LCD library
#include <ESP8266WiFi.h>            //WiFi library
#include <ESP8266HTTPClient.h>      //http client library
#include <WiFiClient.h>             //WiFi client library
#include <WiFiUdp.h>                //UDP library (needed for NTP)
#include <ESPAsyncWebServer.h>      //asynchronous web server library

//pre-processor definitions (pins) _____________________________________________________________________________________________/
#define LIGHT_PIN A0                //light sensor pin
#define TEMP_PIN  D7                //temperature sensor pin

//pre-processor definitions (LCD characters) ___________________________________________________________________________________/
#define CHAR_DEGC byte(0)           //LCD char °C
#define CHAR_ONE0 byte(1)           //LCD char _10
#define CHAR_TWO3 byte(2)           //LCD char _23
#define CHAR_SIG0 byte(3)           //LCD chan signal level 0
#define CHAR_SIG1 byte(4)           //LCD chan signal level 1
#define CHAR_SIG2 byte(5)           //LCD chan signal level 2
#define CHAR_SIG3 byte(6)           //LCD chan signal level 3

//pre-processor definitions (NTP) ______________________________________________________________________________________________/
#define NTP_PORT              2390  //NTP network port
#define MIN_INTERVAL_NTP 1200000UL  //NTP minimum time between synchronizations (not manually requested)

//variables definitions (hardware objects) _____________________________________________________________________________________/
RTC_DS1307 RTC;
LiquidCrystal lcd(D6,D5,D4,D3,D0,D8);
OneWire ds(TEMP_PIN);

//variables definitions (WiFi) _________________________________________________________________________________________________/
String wifi_ssid="ssid";
String wifi_pass="pass";
bool wifi_lastConnected=0;

//variables definitions (data upload) __________________________________________________________________________________________/
bool toSendData=0;
unsigned long lastSent;

//variables definitions (date & time) __________________________________________________________________________________________/
String monthNames[12]={"gen","feb","mar","apr","mag","giu","lug","ago","set","ott","nov","dic"};
int lastSecond=0;

//variables definitions (sensors readings) _____________________________________________________________________________________/
float temperature=0;
int light=0;

//variables definition (serial communication) __________________________________________________________________________________/
String inputString;
bool stringComplete;

//variables definitions (NTP) __________________________________________________________________________________________________/
IPAddress timeServerIP;
const char* ntpServerName="0.europe.pool.ntp.org";
const int NTP_PACKET_SIZE=48;
byte packetBuffer[NTP_PACKET_SIZE];
WiFiUDP udp;
unsigned long NTP_sentPacket;
bool packetRead=1;
bool timeIsSynced;
bool toRenewSync;
bool syncNow=1;
unsigned long timeZoneDelay=3600;

//variables definitions (webServer) ____________________________________________________________________________________________/
AsyncWebServer server(80);
const char index_html[] PROGMEM=R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; }
    .units { font-size: 1.2rem; }
    .dht-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
  </style>
</head>
<body>
  <h2>PON sensors</h2>
  <p>
    <i class="fas fa-thermometer-half" style="color:#059e8a;"></i>
    <span class="dht-labels">Temperature</span>
    <span id="temperature">%TEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <i class="fas fa-sun" style="color:#eedd00;"></i>
    <span class="dht-labels">Light</span>
    <span id="light">%HUMIDITY%</span>
    <sup class="units">/1023</sup>
  </p>
</body>
<script>
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
}, 10000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("light").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/light", true);
  xhttp.send();
}, 5000 ) ;
</script>
</html>)rawliteral";

String processor(const String& var)
{
  if(var=="TEMPERATURE")
    return String(temperature,1);
  else if(var=="LIGHT")
    return String(light);
  return String();
}

//variables definitions (LCD characters) _______________________________________________________________________________________/
byte degC[8]=
{
  B11000,
  B11000,
  B00011,
  B00100,
  B00100,
  B00100,
  B00011,
  B00000,
};

byte one0[8]=
{
  B00000,
  B00000,
  B00000,
  B10111,
  B10101,
  B10101,
  B10101,
  B10111,
};

byte two3[8]=
{
  B00000,
  B00000,
  B00000,
  B11011,
  B01001,
  B11011,
  B10001,
  B11011,
};

byte sig0[8] = {
  B11100,
  B11100,
  B01000,
  B01000,
  B01000,
  B01000,
  B01000,
  B00000,
};

byte sig1[8] = {
  B11100,
  B11100,
  B01000,
  B01000,
  B01000,
  B01011,
  B01011,
  B00000,
};

byte sig2[8] = {
  B00000,
  B00000,
  B00000,
  B11000,
  B11000,
  B11000,
  B11000,
  B00000,
};

byte sig3[8] = {
  B00000,
  B00011,
  B00011,
  B11011,
  B11011,
  B11011,
  B11011,
  B00000,
};

//function that reads temperature from sensor __________________________________________________________________________________/
float getTemperature()
{
  byte data[12];
  byte addr[8];

  if(!ds.search(addr))
  {
    ds.reset_search();
    return -1000;
  }

  if(OneWire::crc8(addr,7)!=addr[7])
    return -1000;

  if(addr[0]!=0x10 && addr[0] != 0x28)
    return -1000;

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);
  byte present=ds.reset();
  ds.select(addr);
  ds.write(0xBE);

  for(int i=0;i<9;i++)
    data[i]=ds.read();

  ds.reset_search();
  byte MSB=data[1];
  byte LSB=data[0];
  float tempRead=((MSB<<8)|LSB);
  float TemperatureSum=tempRead/16;

  return TemperatureSum;
}

//function that sends NTP request ______________________________________________________________________________________________/
unsigned long sendNTPpacket(IPAddress& address)
{
  memset(packetBuffer,0,NTP_PACKET_SIZE);
  packetBuffer[0]=0b11100011;      //LI, Version, Mode
  packetBuffer[1]=0;               //Stratum, or type of clock
  packetBuffer[2]=6;               //Polling Interval
  packetBuffer[3]=0xEC;            //Peer Clock Precision
  packetBuffer[12]=49;
  packetBuffer[13]=0x4E;
  packetBuffer[14]=49;
  packetBuffer[15]=52;
  udp.beginPacket(address,123);    //NTP requests are to port 123
  udp.write(packetBuffer,NTP_PACKET_SIZE);
  udp.endPacket();
}

//setup function _______________________________________________________________________________________________________________/
void setup()
{
  //pinMode __________________________________________________________________________________________________________/
  pinMode(LIGHT_PIN,INPUT);

  //serial communication initialization ______________________________________________________________________________/
  Serial.begin(115200);
  Serial.println();

  //i2c communication initialization _________________________________________________________________________________/
  Wire.begin();

  //RTC initialization _______________________________________________________________________________________________/
  RTC.begin();
  if(!RTC.isrunning())
    RTC.adjust(DateTime(__DATE__,__TIME__));

  //LCD initialization _______________________________________________________________________________________________/
  lcd.begin(16,2);
  lcd.createChar(0,degC);
  lcd.createChar(1,one0);
  lcd.createChar(2,two3);
  lcd.createChar(3,sig0);
  lcd.createChar(4,sig1);
  lcd.createChar(5,sig2);
  lcd.createChar(6,sig3);

  //boot splash ______________________________________________________________________________________________________/
  lcd.setCursor(0,0);
  lcd.print(String(F("Data logger v"))+String(F(VERSION)));
  lcd.setCursor(0,1);
  lcd.print(String(F("Michele Pirola")));
  delay(2000);
  lcd.clear();
  lcd.setCursor(0,0);

  //WiFi connection __________________________________________________________________________________________________/
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid.c_str(), wifi_pass.c_str());
  Serial.println("connecting to WiFi");

  //webServer initialization _________________________________________________________________________________________/
  server.begin();

  server.on("/",HTTP_GET,[](AsyncWebServerRequest *request)
  {
    request->send_P(200,"text/html",index_html,processor);
  });

  server.on("/temperature",HTTP_GET,[](AsyncWebServerRequest *request)
  {
    request->send_P(200,"text/plain",String(temperature,1).c_str());
  });

  server.on("/light",HTTP_GET,[](AsyncWebServerRequest *request)
  {
    request->send_P(200,"text/plain",String(light).c_str());
  });

  //NTP initialization _______________________________________________________________________________________________/
  udp.begin(NTP_PORT);

  //LCD clear, ready to run __________________________________________________________________________________________/
  lcd.clear();
}

//loop function ________________________________________________________________________________________________________________/
void loop()
{
  //check WiFi status ________________________________________________________________________________________________/
  if(WiFi.status()!=WL_CONNECTED && wifi_lastConnected)
  {
    Serial.println("WiFi disconnected");
  }
  else if(WiFi.status()==WL_CONNECTED && !wifi_lastConnected)
  {
    Serial.println("WiFi connected");
  }
  wifi_lastConnected=(WiFi.status()==WL_CONNECTED);

  //check if there are serial commands _______________________________________________________________________________/
  while(Serial.available())
  {
    char inChar=(char)Serial.read();
    inputString+=inChar;
    if(inChar=='\n' || inChar=='\r')
    {
      stringComplete=1;
      inputString.remove(inputString.length()-1); //get rid of \n or \r at the end of the string
    }
  }

  //respond to serial commands _______________________________________________________________________________________/
  if(stringComplete)
  {
    Serial.println("___"+inputString);
    if(inputString=="s")
      syncNow=1;
    else if(inputString=="t")
      Serial.println("temperature="+String(temperature,1)+"°C");
    else if(inputString=="l")
      Serial.println("light="+String(light)+"/1023");
    else if(inputString=="a")
      Serial.println(WiFi.localIP());
    inputString="";
    stringComplete=0;
  }

  //read date and time from the RTC __________________________________________________________________________________/
  DateTime now=RTC.now();

  //correction for daylight saving time ______________________________________________________________________________/
  //calculate DST from date/time instead of changing delay when needed ------------------------------------------- TODO
  if(timeZoneDelay==3600UL && now.month()==3 && now.day()>=25 && now.dayOfTheWeek()==0 && now.hour()==2)
  {
    RTC.adjust(DateTime(RTC.now().unixtime()+3600UL));
    timeZoneDelay=7200UL;
  }
  else if(timeZoneDelay==7200UL && now.month()==10 && now.day()>=25 && now.dayOfTheWeek()==0 && now.hour()==3)
  {
    RTC.adjust(DateTime(RTC.now().unixtime()-3600UL));
    timeZoneDelay=3600UL;
  }

  //NTP sync _________________________________________________________________________________________________________/
  if(((now.minute()==30 && toRenewSync && millis()-NTP_sentPacket>MIN_INTERVAL_NTP) || syncNow) && WiFi.status()==WL_CONNECTED)
  {
    WiFi.hostByName(ntpServerName,timeServerIP);
    sendNTPpacket(timeServerIP);
    NTP_sentPacket=millis();
    packetRead=0;
    toRenewSync=0;
    syncNow=0;
  }
  else if(now.minute()!=30)
    toRenewSync=1;

  if(!packetRead && millis()-NTP_sentPacket>1000)
  {
    int cb=udp.parsePacket();
    if(cb)
    {
      udp.read(packetBuffer,NTP_PACKET_SIZE);
      unsigned long highWord=word(packetBuffer[40],packetBuffer[41]);
      unsigned long lowWord=word(packetBuffer[42],packetBuffer[43]);
      unsigned long secsSince1900=highWord<<16|lowWord;
      const unsigned long seventyYears=2208988800UL;
      unsigned long epoch=secsSince1900-seventyYears;
      Serial.print("ntp ok\n");
      timeIsSynced=1;
      RTC.adjust(DateTime(epoch+timeZoneDelay));
    }
    else
    {
      Serial.print("ntp failed\n");
      timeIsSynced=0;
    }
    packetRead=1;
  }

  //read sensors data and print them on LCD together with WiFi signal level __________________________________________/
  if(now.second()!=lastSecond)
  {
    temperature=getTemperature();

    light=analogRead(LIGHT_PIN);

    lcd.setCursor(0,1);

    if(temperature>=0 && temperature<10)
      lcd.print(F("  "));
    else if(temperature>=10 || (temperature<0 && temperature>-10))
      lcd.print(F(" "));

    lcd.print(temperature,1);

    lcd.write(CHAR_DEGC);

    lcd.setCursor(7,1);
    if(WiFi.status()!=WL_CONNECTED)
    {
      lcd.write(CHAR_SIG0);
      lcd.print("x");
      timeIsSynced=0;
      syncNow=1;
    }
    else
    {
      if(WiFi.RSSI()>-65)
      {
        lcd.write(CHAR_SIG1);
        lcd.write(CHAR_SIG3);
      }
      else if(WiFi.RSSI()>-70)
      {
        lcd.write(CHAR_SIG1);
        lcd.write(CHAR_SIG2);
      }
      else if(WiFi.RSSI()>-80)
      {
        lcd.write(CHAR_SIG1);
        lcd.print(" ");
      }
      else
      {
        lcd.write(CHAR_SIG0);
        lcd.print(" ");
      }
    }

    lcd.setCursor(9,1);

    if(light<1000)
      lcd.print(F(" "));

    if(light<100)
      lcd.print(F(" "));

    if(light<10)
      lcd.print(F(" "));

    lcd.print(light);

    lcd.print(F("/"));
    lcd.write(CHAR_ONE0);
    lcd.write(CHAR_TWO3);

    lastSecond=now.second();
  }

  //print date and time on LCD _______________________________________________________________________________________/
  lcd.setCursor(0,0);

  if(now.hour()<10)
    lcd.print(F("0"));
  lcd.print(now.hour());

  lcd.print(F(":"));

  if(now.minute()<10)
    lcd.print(F("0"));
  lcd.print(now.minute());

  lcd.print(F(":"));

  if(now.second()<10)
    lcd.print(F("0"));
  lcd.print(now.second());

  if(timeIsSynced)
    lcd.print(F(" "));
  else
    lcd.print(F("?"));

  if(now.day()<10)
    lcd.print(F("0"));
  lcd.print(now.day());

  lcd.print(monthNames[now.month()-1]);

  if(now.year()-2000<10)
    lcd.print(F("0"));
  lcd.print(now.year()-2000);

  lcd.print(F(" "));

  //upload sensors data to server ____________________________________________________________________________________/
  if(now.minute()==0)
  {
    if(toSendData)
    {
      WiFiClient client;
      HTTPClient http;
      int anno=now.year();
      int mese=now.month();
      int giorno=now.day();
      int ore=now.hour();
      int min=now.minute();
      int sec=now.second();
      bool dst=(timeZoneDelay==7200UL);
      float t=temperature;
      int l=light;
      int flag=0;
      unsigned long key=42;

      String url="http://www2.pv.infn.it/~andrea/monclima/insert.php?Anno="+String(anno)+
                  "&Mese="+String(mese)+
                  "&Giorno="+String(giorno)+
                  "&Ore="+String(ore)+
                  "&Minuti="+String(min)+
                  "&Secondi="+String(sec)+
                  "&Estivo="+String(dst)+
                  "&Temperatura="+String(t,1)+
                  "&Luce="+String(l)+
                  "&Flag="+String(flag)+
                  "&Chiave="+String(key);

      if(http.begin(client,url))
      {
        int httpCode=http.GET();
        if(httpCode>0)
        {
          Serial.printf("http get code: %d\n",httpCode);
          if(httpCode==HTTP_CODE_OK || httpCode==HTTP_CODE_MOVED_PERMANENTLY)
          {
            String payload=http.getString();
            Serial.println(payload);
          }
        }
        else
          Serial.printf("http get failed, error: %s\n",http.errorToString(httpCode).c_str());

        http.end();
      }
      else
        Serial.println("http connection failed");
    }

    toSendData=0;
    lastSent=millis();
  }
  else
    toSendData=1;
}
